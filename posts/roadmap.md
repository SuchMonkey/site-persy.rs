---
layout: post.liquid

title: Roadmap
published_date: 2018-03-05 13:00:00 -0500
---


## Persy 0.9
<span class="todo">x </span> Make public the snapshot API  
<span class="todo">x </span> Remove of API deprecated in previous version  
<span class="todo">x </span> Evaluate the Implementation for reuse of empty segment pages  


## Persy 0.10
<span class="todo">x </span> General API review and stabilization   
<span class="todo">x </span> review & examples of concurrency management   
<span class="todo">x </span> Evolve recover API for more complex use cases



## Persy 1.0

<span class="todo">x </span> Exists a community of users that have done some testing   
<span class="todo">x </span> Every thing before actually works   

## Persy 1.x

<span class="todo">x </span> Advanced caching algorithms  
<span class="todo">x </span> Introduction of possible async disc operations   
