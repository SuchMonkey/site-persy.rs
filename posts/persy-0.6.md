---
layout: post.liquid

title: Persy 0.6
published_date: 2019-08-26 00:00:30 +0100
---

Again in just roughly 3 months a new release of Persy is out, this include a good set of improvements.

## What is new in Persy 0.6
Indexes gained the support of double ended iterator for ranges lookup API, and the support for a new set of index-able primitive types: floats and 128 bytes sizes.  
All the iterators "tx-aware" for both indexes and segments now support the access to the transaction they are borrowing, allowing to do transactional operations during the iteration.  
There have been also some other optimization and fix in few other areas, like the first base support for release of not used disc space, shrinking the file and the support of path on open/create APIs.  
On the performance side, some benchmark where run and some simple optimization where done,  now it avoid to write too much data on the disc when multiple index updates happen in a single transaction.  

For the external tools some time ago was released Persy ExpImp (Export Import) for version 0.5 of Persy, this tool allow to export all your data to a JSON format, soon after the Persy 0.6 release will be released a new version of the export import tool that
will support importing data exported from Persy 0.5 to  Persy 0.6 storages, so finally you can upgrade to new version without manually export and import data.  
  
## Future plans
For future development are not planned big changes, there will be some optimization for performance, more tests, some review for improve consistency and clarity of the API.  
The only new API that may land in future could be a public API of snapshots, more work will be also done on concurrent transaction isolation.

