---
layout: post.liquid

title: Persy 0.8
published_date: 2019-11-20 00:00:30 +0100
---

Quite quickly a new release did build up, in less than two months we managed to merge a quite big pull request, and do some quite big changes in the engine.


## What is new in Persy 0.8

The first most important change landed was the redesign of the public API for transaction, the new way implemented by Erik Zscheile is actually more ergonomic and play in a better way with the ownership mechanism of rust,
I did play a bit with the new way in some downstream project and i found it easier and smother to use.
The old API are still there but they are deprecated, they will probably be removed in the next release, so you can update to this version without problem and do the migration to the new API smoothly and as far as you remove all the deprecated usage, you will be able to update to the next version without problems

Other big changes are all about parallelism and performances, in terms of parallelism the previous version were keeping an exclusive lock on the index modified for all the time from the `prepare_commit` to the final `commit` even though this lock was only for modification, the read in parallel were allowed it was a pretty big lock.
Since this version the lock is kept just for a range of keys around the modified key, so allowing to have multiple threads modifying keys that do not seat in the same range, for the same index.

Other improvement where done in the logging, so now operations are logged in a way that I could removed few fsync and only one fsync is left at the prepare_commit keeping the storage still safe.
from that also other minor changes where done over all to avoid not needed clone or duplicated operations to gain some speedup in performance, even thought still this storage is aiming to be more safe than fast.
In any case if you did some benchmarks in the past I would suggest to double check again them, should be faster now even thought not sky-rocketing fast.


## Future Plans

As said in previous release the only public API really missing is the snapshot API, this will be include probably in the next release and will be the biggest change of the release,
additional speed improvement and minor changes will be included as well as usual.

From this step we should getting close to the stabilization and the 1.0.0, even though some more changes will happen in the implementation to allow better testing of the resiliency.



