---
layout: post.liquid

title: Persy 0.2
published_date: 2017-09-12 21:00:30 -0500
---

The second step for a single file storage engine completely written in rust is done.

## What can be done with Persy 0.2

- Everything that could be done with [0.1](/posts/persy-0.1.html)
- Scan of segment with access to the record identifier for handle updates
- Scan of segment in transaction including the transaction changes
- Finer control of concurrent update with concurrency strategies
- Reduced number of flush needed in transaction commit
- Reduced log size using segment ids instead of names
- Full support for Multi-thread operation on segments

### Still a long path

Few things have been stabilized in this release and the API start to looks final, lot of more things need to be done 
and a lot of more thing are cool to have included in the storage, like for example indexing support, at this level anyway the storage
can be safely used as a backend for a simple database, to be aware though that file binary compatibility won't be guaranteed till 1.0


