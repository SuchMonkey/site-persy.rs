---
layout: post.liquid

title: Old Releases
published_date: 2018-03-04 21:00:30 -0500
---
 
## 0.7

[Release 0.7 ](/posts/persy-0.7.html)  
[ Getting Started 0.7 ](/posts/getting-started-0.7.html)  

## 0.6

[Release 0.6 ](/posts/persy-0.6.html)  
[ Getting Started 0.6 ](/posts/getting-started-0.6.html)  

## 0.5

[Release 0.5 ](/posts/persy-0.5.html)  
[ Getting Started 0.5 ](/posts/getting-started-0.5.html)  

## 0.4

[Release 0.4 ](/posts/persy-0.4.html)  
[ Getting Started 0.4 ](/posts/getting-started-0.4.html)  

## 0.3

[Release 0.3 ](/posts/persy-0.3.html)  
[ Getting Started 0.3 ](/posts/getting-started-0.3.html)  


## 0.2

[Release 0.2 ](/posts/persy-0.2.html)  
[ Getting Started 0.2 ](/posts/getting-started-0.2.html)  


## 0.1

[Release 0.1 ](/posts/persy-0.1.html)  
[ Getting Started 0.1 ](/posts/getting-started-0.1.html)  

